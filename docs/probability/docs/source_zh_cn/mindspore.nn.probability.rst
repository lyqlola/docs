mindspore.nn.probability
================================

概率。

用于构建概率网络的高级组件。

Bayesian Layers
---------------

.. mscnplatformautosummary::
    :toctree: nn_probability
    :nosignatures:
    :template: classtemplate_probability.rst

    mindspore.nn.probability.bnn_layers.ConvReparam
    mindspore.nn.probability.bnn_layers.DenseLocalReparam
    mindspore.nn.probability.bnn_layers.DenseReparam

Prior and Posterior Distributions
----------------------------------

.. mscnplatformautosummary::
    :toctree: nn_probability
    :nosignatures:
    :template: classtemplate_probability.rst

    mindspore.nn.probability.bnn_layers.NormalPosterior
    mindspore.nn.probability.bnn_layers.NormalPrior

Bayesian Wrapper Functions
---------------------------

.. mscnplatformautosummary::
    :toctree: nn_probability
    :nosignatures:
    :template: classtemplate_probability.rst

    mindspore.nn.probability.bnn_layers.WithBNNLossCell

Deep Probability Networks
--------------------------

.. mscnplatformautosummary::
    :toctree: nn_probability
    :nosignatures:
    :template: classtemplate_probability.rst

    mindspore.nn.probability.dpn.ConditionalVAE
    mindspore.nn.probability.dpn.VAE

Infer
------

.. mscnplatformautosummary::
    :toctree: nn_probability
    :nosignatures:
    :template: classtemplate_probability.rst

    mindspore.nn.probability.infer.ELBO
    mindspore.nn.probability.infer.SVI

ToolBox
---------

.. mscnplatformautosummary::
    :toctree: nn_probability
    :nosignatures:
    :template: classtemplate_probability.rst

    mindspore.nn.probability.toolbox.UncertaintyEvaluation
    mindspore.nn.probability.toolbox.VAEAnomalyDetection

Model Transformer
------------------

.. mscnplatformautosummary::
    :toctree: nn_probability
    :nosignatures:
    :template: classtemplate_probability.rst

    mindspore.nn.probability.transforms.TransformToBNN
