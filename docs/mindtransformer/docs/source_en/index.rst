MindSpore Transformer Document
==============================

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Installation Deployment

   mindtransformer_install

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: BERT Fine adjustment

   mindtransformer_bert_finetune

.. toctree::
   :maxdepth: 1
   :caption: API References

   mindtransformer

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: RELEASE NOTES

   RELEASE