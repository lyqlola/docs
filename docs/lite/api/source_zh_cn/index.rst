.. MindSpore documentation master file, created by
   sphinx-quickstart on Thu Mar 24 10:00:00 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MindSpore Lite API
=======================

.. toctree::
   :maxdepth: 1
   :caption: C++ API

   api_cpp/mindspore
   api_cpp/mindspore_dataset
   api_cpp/mindspore_dataset_transforms
   api_cpp/mindspore_kernel
   api_cpp/mindspore_registry
   api_cpp/mindspore_registry_opencl
   api_cpp/mindspore_converter
   api_cpp/lite_cpp_example



.. toctree::
   :maxdepth: 1
   :caption: JAVA API

   api_java/class_list
   api_java/model
   api_java/mscontext
   api_java/mstensor
   api_java/graph
   api_java/runner_config
   api_java/model_parallel_runner
   api_java/lite_java_example

.. toctree::
   :maxdepth: 1
   :caption: C API

   api_c/context_c
   api_c/data_type_c
   api_c/format_c
   api_c/model_c
   api_c/tensor_c
   api_c/types_c
   api_c/lite_c_example