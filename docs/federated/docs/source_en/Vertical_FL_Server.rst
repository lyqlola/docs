Vertical FL-Server
=====================

.. toctree::
   :maxdepth: 1

   vertical/vertical_communicator
   vertical/vertical_federated_FLModel
   vertical/vertical_federated_yaml